create or replace function add_provtuple(
	tablename regclass
)
returns void
as $$
declare
	curid integer := 0;
	tupleid text;
	_ctid tid;
begin
	EXECUTE format('alter table %s add column if not exists provtuple varchar(20)', tablename);
	for _ctid in execute 'select ctid from ' || tablename for update
	loop
		tupleid = 't' || curid;
		EXECUTE format('update %s
		set provtuple = ''%s''
		where ctid = $1', tablename, tupleid)
		using _ctid;

		curid = curid + 1;
	end loop;
end; $$

language plpgsql;

