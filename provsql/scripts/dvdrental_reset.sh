#!/bin/sh
set "PGPASSWORD=m"
dropdb dvdrental
createdb dvdrental
pg_restore -U joker -d dvdrental /home/joker/sql-provenance/provsql/scripts/dvdrental.tar
psql dvdrental < ../config/setup.sql
psql dvdrental < ../config/func.sql

git pull

python3 ../src/provenance.py -q ../config/dvdrental_query.in -o dvdrental_output.txt ../config/dvdrental_config.txt
