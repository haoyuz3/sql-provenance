--
-- PostgreSQL database dump
--

-- Dumped from database version 11.9
-- Dumped by pg_dump version 11.9

-- Started on 2020-10-14 16:54:33

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 199 (class 1259 OID 17072)
-- Name: brands; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.brands (
    brandname character varying(255) NOT NULL,
    yearestablished integer,
    ceo character varying(255)
);


ALTER TABLE public.brands OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 17093)
-- Name: courses; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.courses (
    crn integer NOT NULL,
    title character varying(255),
    department character varying(100),
    instructor character varying(255)
);


ALTER TABLE public.courses OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 17051)
-- Name: customers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.customers (
    customerid integer NOT NULL,
    firstname character varying(255),
    lastname character varying(255),
    phonenumber real
);


ALTER TABLE public.customers OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 17088)
-- Name: enrollments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.enrollments (
    netid character varying(10) NOT NULL,
    crn integer NOT NULL,
    credits integer,
    score real
);


ALTER TABLE public.enrollments OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 17064)
-- Name: products; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.products (
    productid integer NOT NULL,
    productname character varying(255),
    brandname character varying(255),
    yearreleased integer
);


ALTER TABLE public.products OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 17059)
-- Name: purchases; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.purchases (
    purchaseid integer NOT NULL,
    customerid integer,
    productid integer,
    price integer
);


ALTER TABLE public.purchases OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 17080)
-- Name: students; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.students (
    netid character varying(10) NOT NULL,
    firstname character varying(255),
    lastname character varying(255),
    department character varying(100)
);


ALTER TABLE public.students OWNER TO postgres;

--
-- TOC entry 2850 (class 0 OID 17072)
-- Dependencies: 199
-- Data for Name: brands; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.brands VALUES ('Apple', 1976, 'Tim Cook');
INSERT INTO public.brands VALUES ('Samsung', 1938, 'Ki Nam Kim, Hyun Suk Kim, Dong Jin Koh');
INSERT INTO public.brands VALUES ('OnePlus', 2013, 'Pete Lau');
INSERT INTO public.brands VALUES ('Google', 1998, 'Sundar Pichai');
INSERT INTO public.brands VALUES ('Honor', 2013, 'George Zhao');


--
-- TOC entry 2853 (class 0 OID 17093)
-- Dependencies: 202
-- Data for Name: courses; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.courses VALUES (124, 'Deep Learning', 'CS', 'Andrew Ng');
INSERT INTO public.courses VALUES (125, 'Data Base', 'CS', 'Abdu Alawini');
INSERT INTO public.courses VALUES (126, 'Fieds and Waves', 'ECE', 'Erhan Kudeki');
INSERT INTO public.courses VALUES (127, 'Computer System', 'ECE', 'Michael Bailey');


--
-- TOC entry 2847 (class 0 OID 17051)
-- Dependencies: 196
-- Data for Name: customers; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.customers VALUES (0, 'Eleanora', 'Lacey', 3.22448205e+09);
INSERT INTO public.customers VALUES (1, 'Wesley', 'Herman', 3.81870438e+09);
INSERT INTO public.customers VALUES (2, 'Elfreda', 'Patrick', 6.15243469e+09);
INSERT INTO public.customers VALUES (3, 'James', 'Grenville', 4.60469709e+09);
INSERT INTO public.customers VALUES (4, 'Jayme', 'Castle', 9.88806861e+09);
INSERT INTO public.customers VALUES (5, 'Christopher', 'Ryley', 6.26915123e+09);
INSERT INTO public.customers VALUES (6, 'Barry', 'Easom', 9.94693427e+09);
INSERT INTO public.customers VALUES (7, 'Aretha', 'Tyson', 1.04360685e+09);
INSERT INTO public.customers VALUES (8, 'Josh', 'Derrickson', 4.90183168e+09);
INSERT INTO public.customers VALUES (9, 'Rebecca', 'Harlan', 3.02250829e+09);
INSERT INTO public.customers VALUES (10, 'Lowell', 'Best', 8.35596032e+09);
INSERT INTO public.customers VALUES (11, 'Jannah', 'Gladyn', 3.09791693e+09);
INSERT INTO public.customers VALUES (12, 'Ellie', 'Ball', 5.52846592e+09);
INSERT INTO public.customers VALUES (13, 'Jenna', 'Watts', 9.97023846e+09);
INSERT INTO public.customers VALUES (14, 'Stacey', 'Anthonyson', 2.87751373e+09);
INSERT INTO public.customers VALUES (15, 'Jez', 'Hopper', 4.27223347e+09);
INSERT INTO public.customers VALUES (16, 'Janet', 'Ogden', 6.75851315e+09);
INSERT INTO public.customers VALUES (17, 'Shonda', 'Winchester', 2.49113754e+09);
INSERT INTO public.customers VALUES (18, 'Kelly', 'Jerome', 5.99478477e+09);
INSERT INTO public.customers VALUES (19, 'Bethany', 'Barnet', 1.80941786e+09);
INSERT INTO public.customers VALUES (20, 'Deven', 'Miles', 8.29361766e+09);
INSERT INTO public.customers VALUES (21, 'Haylie', 'Roy', 1.79204992e+09);
INSERT INTO public.customers VALUES (22, 'Adriana', 'Mendez', 6.3412649e+09);
INSERT INTO public.customers VALUES (23, 'Luisa', 'Alamilla', 5.97548544e+09);
INSERT INTO public.customers VALUES (24, 'Luis', 'Alamilla', 5.98341171e+09);


--
-- TOC entry 2852 (class 0 OID 17088)
-- Dependencies: 201
-- Data for Name: enrollments; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.enrollments VALUES ('tswift2', 124, 4, 90);
INSERT INTO public.enrollments VALUES ('tswift2', 125, 3, 96);
INSERT INTO public.enrollments VALUES ('tswift2', 126, 3, 88);
INSERT INTO public.enrollments VALUES ('tswift2', 127, 4, 92);
INSERT INTO public.enrollments VALUES ('esheeran5', 124, 4, 70);
INSERT INTO public.enrollments VALUES ('esheeran5', 125, 3, 78);
INSERT INTO public.enrollments VALUES ('tjones9', 125, 3, 60);
INSERT INTO public.enrollments VALUES ('tjones9', 127, 4, 77);
INSERT INTO public.enrollments VALUES ('ssmith6', 124, 4, 87);
INSERT INTO public.enrollments VALUES ('bmars4', 124, 4, 99);


--
-- TOC entry 2849 (class 0 OID 17064)
-- Dependencies: 198
-- Data for Name: products; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.products VALUES (0, 'iPhone 7', 'Apple', 2016);
INSERT INTO public.products VALUES (1, 'iPhone 8', 'Apple', 2017);
INSERT INTO public.products VALUES (2, 'iPhone X', 'Apple', 2017);
INSERT INTO public.products VALUES (3, 'iPhone XS', 'Apple', 2018);
INSERT INTO public.products VALUES (4, 'iPhone XR', 'Apple', 2018);
INSERT INTO public.products VALUES (5, 'Samsung Galaxy S10', 'Samsung', 2019);
INSERT INTO public.products VALUES (6, 'Samsung Galaxy Note 9', 'Samsung', 2018);
INSERT INTO public.products VALUES (7, 'OnePlus 7 Pro', 'OnePlus', 2019);
INSERT INTO public.products VALUES (8, 'Google Pixel 3 XL', 'Google', 2018);
INSERT INTO public.products VALUES (9, 'Honor View 20', 'Honor', 2019);


--
-- TOC entry 2848 (class 0 OID 17059)
-- Dependencies: 197
-- Data for Name: purchases; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.purchases VALUES (0, 11, 4, 426);
INSERT INTO public.purchases VALUES (1, 12, 3, 129);
INSERT INTO public.purchases VALUES (2, 11, 5, 675);
INSERT INTO public.purchases VALUES (3, 8, 3, 872);
INSERT INTO public.purchases VALUES (4, 16, 9, 500);
INSERT INTO public.purchases VALUES (5, 0, 0, 123);
INSERT INTO public.purchases VALUES (6, 19, 1, 100);
INSERT INTO public.purchases VALUES (7, 20, 5, 780);
INSERT INTO public.purchases VALUES (8, 22, 2, 716);
INSERT INTO public.purchases VALUES (9, 11, 5, 951);
INSERT INTO public.purchases VALUES (10, 22, 1, 165);
INSERT INTO public.purchases VALUES (11, 0, 7, 569);
INSERT INTO public.purchases VALUES (12, 8, 8, 556);
INSERT INTO public.purchases VALUES (13, 19, 2, 813);
INSERT INTO public.purchases VALUES (14, 17, 8, 175);
INSERT INTO public.purchases VALUES (15, 3, 2, 51);
INSERT INTO public.purchases VALUES (16, 2, 2, 750);
INSERT INTO public.purchases VALUES (17, 5, 1, 957);
INSERT INTO public.purchases VALUES (18, 14, 2, 373);
INSERT INTO public.purchases VALUES (19, 13, 1, 439);
INSERT INTO public.purchases VALUES (20, 11, 7, 215);
INSERT INTO public.purchases VALUES (21, 0, 6, 395);
INSERT INTO public.purchases VALUES (22, 7, 4, 476);
INSERT INTO public.purchases VALUES (23, 21, 0, 928);
INSERT INTO public.purchases VALUES (24, 19, 1, 706);
INSERT INTO public.purchases VALUES (25, 20, 0, 466);
INSERT INTO public.purchases VALUES (26, 21, 5, 553);
INSERT INTO public.purchases VALUES (27, 5, 2, 232);
INSERT INTO public.purchases VALUES (28, 5, 2, 851);
INSERT INTO public.purchases VALUES (29, 6, 2, 401);
INSERT INTO public.purchases VALUES (30, 1, 4, 739);
INSERT INTO public.purchases VALUES (31, 0, 6, 889);
INSERT INTO public.purchases VALUES (32, 16, 4, 472);
INSERT INTO public.purchases VALUES (33, 9, 2, 876);
INSERT INTO public.purchases VALUES (34, 2, 9, 104);
INSERT INTO public.purchases VALUES (35, 4, 8, 410);
INSERT INTO public.purchases VALUES (36, 13, 8, 571);
INSERT INTO public.purchases VALUES (37, 22, 8, 598);
INSERT INTO public.purchases VALUES (38, 24, 7, 236);
INSERT INTO public.purchases VALUES (39, 4, 3, 449);
INSERT INTO public.purchases VALUES (40, 12, 3, 727);
INSERT INTO public.purchases VALUES (41, 8, 4, 734);
INSERT INTO public.purchases VALUES (42, 15, 9, 444);
INSERT INTO public.purchases VALUES (43, 22, 8, 319);
INSERT INTO public.purchases VALUES (44, 11, 1, 958);
INSERT INTO public.purchases VALUES (45, 17, 0, 721);
INSERT INTO public.purchases VALUES (46, 10, 3, 304);
INSERT INTO public.purchases VALUES (47, 18, 9, 664);
INSERT INTO public.purchases VALUES (48, 23, 7, 645);
INSERT INTO public.purchases VALUES (49, 1, 5, 657);
INSERT INTO public.purchases VALUES (50, 11, 4, 500);


--
-- TOC entry 2851 (class 0 OID 17080)
-- Dependencies: 200
-- Data for Name: students; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.students VALUES ('tswift2', 'Taylor', 'Swift', 'CS');
INSERT INTO public.students VALUES ('esheeran5', 'Ed', 'Sheeran', 'ECE');
INSERT INTO public.students VALUES ('ssmith6', 'Sam', 'Smith', 'ECE');
INSERT INTO public.students VALUES ('tjones9', 'Tom', 'Jones', 'ECE');
INSERT INTO public.students VALUES ('bmars4', 'Bruno', 'Mars', 'Physics');


--
-- TOC entry 2719 (class 2606 OID 17079)
-- Name: brands brands_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.brands
    ADD CONSTRAINT brands_pkey PRIMARY KEY (brandname);


--
-- TOC entry 2725 (class 2606 OID 17100)
-- Name: courses courses_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.courses
    ADD CONSTRAINT courses_pkey PRIMARY KEY (crn);


--
-- TOC entry 2713 (class 2606 OID 17058)
-- Name: customers customers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customers
    ADD CONSTRAINT customers_pkey PRIMARY KEY (customerid);


--
-- TOC entry 2723 (class 2606 OID 17092)
-- Name: enrollments enrollments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.enrollments
    ADD CONSTRAINT enrollments_pkey PRIMARY KEY (netid, crn);


--
-- TOC entry 2717 (class 2606 OID 17071)
-- Name: products products_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_pkey PRIMARY KEY (productid);


--
-- TOC entry 2715 (class 2606 OID 17063)
-- Name: purchases purchases_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.purchases
    ADD CONSTRAINT purchases_pkey PRIMARY KEY (purchaseid);


--
-- TOC entry 2721 (class 2606 OID 17087)
-- Name: students students_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.students
    ADD CONSTRAINT students_pkey PRIMARY KEY (netid);


-- Completed on 2020-10-14 16:54:33

--
-- PostgreSQL database dump complete
--

