#!/bin/sh
set "PGPASSWORD=m"
dropdb provdb
createdb provdb
psql provdb < provdb.sql
psql provdb < ../config/setup.sql
psql provdb < ../config/func.sql

git pull

python3 ../src/provenance.py -q ../config/provdb_query.in -o provdb_output.txt ../config/provdb_config.txt
