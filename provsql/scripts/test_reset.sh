#!/bin/sh
dropdb test
createdb test
psql test < ../config/setup.sql
psql test <../config/test.sql
psql test < ../config/func.sql

git pull

python3 ../src/provenance.py -q ../config/subquery.in -o test_output.txt ../config/test_config.txt
