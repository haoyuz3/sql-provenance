def commitAndExecute(conn, cur, command):
    cur.execute(command)
    conn.commit()

# Return formula composed with 𝟙.
def baseFormula(formula):
    charset = ["⊕", "⊗", "⊖", "𝟙", " ", "(", ")"]
    output = ""
    state = 0
    for c in formula:
        if c in charset:
            if state == 1:
                output = output + "𝟙" + c
                state = 0
            else:
                output = output + c
        else:
            if state == 1:
                continue
            else:
                state = 1
    return output
            
def updateBaseFormula(tname, count, base):
    charset = ["⊕", "⊗", "⊖", " ", "(", ")"]
    output = ""
    tmp = 0
    state = 0
    for i, c in enumerate(base):
        if c in charset:
            if state == 1:
                tmp += 1
                state = 0
            output += c
            continue
        elif c == "𝟙":
            if tmp == count:
                output = output + tname
                output = output + base[i+1:]
                break
            else:
                output += c
                tmp += 1
        else:
            if state == 0:
                state = 1
            output += c
    return output
            


def remove1sAndAddTablename(formula, table, base):
    charset = ["⊕", "⊗", "⊖", " ", "(", ")"]
    count = 0
    state = 0
    tname = ""
    for c in formula:
        if c in charset:
           if state == 1:
                state = 0
                base = updateBaseFormula(table + "." + tname, count, base)
                tname = ""
                count += 1
           continue
        elif c == "𝟙":
            count += 1
        else:
            if state == 0:
                state = 1
            tname = tname + c
    return base
    
def compareTwoRows(row1, row2):
    for i in range(1, len(row1)-1):
        if row1[i] != row2[i]:
            return False
    return True

def combineSameRow(input):
    output = []
    lastseen = None
    for row in input:
        if lastseen == None:
            output.append(row)
            lastseen = row
        elif compareTwoRows(row, lastseen):
            tmp = output[len(output)-1]
            tmp[0] = tmp[0] + " ⊕ " + row[0]
            output[len(output)-1] = tmp
        else:
            output.append(row)
            lastseen = row
    return output

