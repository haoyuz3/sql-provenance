SELECT C.CustomerId, C.FirstName, C.LastName, C.PhoneNumber
FROM Customers C NATURAL JOIN Purchases P
WHERE C.CustomerId IN (SELECT C1.CustomerId
					     FROM Customers C1 NATURAL JOIN Purchases P1
					     WHERE P1.Price >=  ALL(SELECT Price
											     FROM Purchases
											     WHERE ProductId = P1.ProductId))
GROUP BY P.CustomerId
ORDER BY P.CustomerId DESC