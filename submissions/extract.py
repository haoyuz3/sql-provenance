import os
import os.path
from os import path
import psycopg2
import traceback
import sys

query = {} # a dictionary (key:questionId, value: User-dictionary(key:UserId, value: Num-dictionary(key:10-digit num, value: submission)))

def commitAndExecute(conn, cur, command):
    cur.execute(command)
    conn.commit()


def extractData():
    mydir = "HW1"
    filesname = os.listdir(mydir)

    for f in filesname:
        # First find questionID
        q_idx = f.find("q")
        t_idx = f.find("_")
        query_idx = f.find("query")

        questionId = 0
        num = 0 # the number of submission for the user for a single question.
        if f[q_idx+1] == "1" and f[q_idx+2] == "0":
            questionId = int(f[q_idx+1 : q_idx+3])
            num = int(f[q_idx+3 : query_idx])
        else:
            questionId = int(f[q_idx+1 : q_idx+2])
            num = int(f[q_idx+2 : query_idx])
        userId = int(f[:t_idx])
          
        # question id not in dictionary.
        if questionId not in query:
            query[questionId] = {}
     
        filepath = mydir + "/" + f
        fl = open(filepath)
        sql_str = fl.read()
        sql_str = sql_str.strip().replace("\n", " ")
        if userId not in query[questionId]:
            query[questionId][userId] = {} 
        query[questionId][userId][num] = sql_str


def main():
    try:
        os.makedirs("hw1-provenance")
    except OSError as e:
        print(e)
    conn = psycopg2.connect("dbname=provdb user=joker password=m")
    cur = conn.cursor()
    extractData()
    #print(query)
    for question_id in [1,2,3,4,5,6,7,8]:
        if question_id != 7:
            continue
        f = open("hw1-provenance/q"+str(question_id)+".txt", "w+")
        question_submissions = query[question_id]
        for student_id in list(question_submissions.keys()):
            submissions = question_submissions[student_id]
            for sub_id in list(submissions.keys()):
                try:
                    commitAndExecute(conn, cur, submissions[sub_id])
                    tmp = cur.fetchall()
                    if len(tmp) != 0:
                        tt = submissions[sub_id]
                        idx = tt.lower().find("order")
                        f.write(tt[:idx]+"\n")
                except (Exception, psycopg2.DatabaseError) as error:
                    print(submissions[sub_id], error)
                conn.commit()
                cur.close()
                conn.close()
                conn = psycopg2.connect("dbname=provdb user=joker password=m")
                cur = conn.cursor()

    f.close()



if __name__ == "__main__":
    # execute only if run as a script
    main()
