import sqlparse as sp
import argparse
import sys
import copy
import time
import operator
import provenanceUtils as ut
import traceback
import re
import pg8000

#When craeting the auxillary provenance tables, need to add a column called provtuple, which is a sorted array
#[t1, t2, t3...]
tablesWithProvenance = {}
user = ""
database = ""
password = ""

def tablesInQuery(conn, query):
    output = conn.run("explain " + query)
    tables = []
    for tmp in output:
        row = tmp[0]
        if "Seq Scan on " in row:
            idx = row.index("Seq Scan on ")
            idx += 12
            table = ""
            for i in range(idx, len(row)):
                if row[i] == " ":
                    break
                table += row[i]
            tables.append(table)
    return tables



#Preprocess query
def preprocess_query(query, conn):
    tables = tablesInQuery(conn, query)
    output = []
    for table in tables:
        if table not in tablesWithProvenance:
            cmdAddProvtuple = "select \"add_provtuple\"('{}')".format(table)
            command1 = "select add_provenance('{}');".format(table)
            command2 = "select create_provenance_mapping('{}_provtuple_mapping','{}','provtuple');".format(table.lower(), table)
            conn.run(cmdAddProvtuple)
            conn.run(command1)
            conn.run(command2)
            tablesWithProvenance[table] = 1
        formula = ", formula(provenance(), '{}_provtuple_mapping') ".format(table.lower())
        #formula = " aggregation_formula(AVG(Score), '{}_provtuple_mapping'), ".format(table.lower())
        index = query.lower().find('from')
        tmp = query[:index] + formula + query[index:]
        #print(tmp)
        output.append(tmp)
    return (tables,output)

#formulas:[[formula1, formula2, ...],
#           [formula1, ...]
#           ...
#         ]
#fs: [formula1, formula2, ...]

def exe_query(query, out_file, conn, file):
    
    print("executing query: " + query)
    tables, queries = preprocess_query(query, conn)
    
    output = query+"@"
    for processed_query in queries:
        output = output+processed_query+"#"
    output += "@"
    for table in tables:
        output = output + table + "#"
    file.write(output + "\n")
    

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-q", "--query", help="Relative path for query file.")
    parser.add_argument("-o", "--output", help="Relative path for output file. Default as stdout")
    parser.add_argument("-d", "--database", help="Database Name")
    parser.add_argument("-u", "--user", help="Username")
    parser.add_argument("-p", "--password", help="Password")
    args = parser.parse_args() 

    queries = []
    out_file = sys.stdout
    global database
    global user
    global password
    if args.database:
        database = args.database
    if args.user:
        user = args.user
    if args.password:
        password = args.password
    if args.query:
        with open(args.query) as fp:
            line = fp.readline()
            while line:
                queries.append(line.strip())
                line = fp.readline()
    else:
        print("Please specify at least one query file.")
        exit(1)

    if args.output:
        out_file = open(args.output, 'w')
    
    conn = None
    print('connecting to databse...')
    conn = pg8000.connect(user, database=database, password=password)
    conn.autocommit = True
    conn.run('SET search_path TO public, provsql;')

    file = open("intermediate", "w+")
    
    for query in queries:
        try:
            exe_query(query, out_file, conn, file)
            print("FINISHED: "+query)
        except (Exception, pg8000.Error) as error:
            print(traceback.format_exc())
            #out_file.write(traceback.format_exc()+"\n")
            conn.commit()
            conn.close()
            conn = pg8000.connect(user, database=database, password=password)
            conn.run('SET search_path TO public, provsql;')
        
            
    print("Done!")
    out_file.close()
    file.close()
    if conn is not None:
        conn.close()
        print('Database connection closed.')

if __name__ == '__main__':
    main()