import sqlparse as sp
import argparse
import sys
import copy
import time
import operator
import provenanceUtils as ut
import traceback
import re
import pg8000

#When craeting the auxillary provenance tables, need to add a column called provtuple, which is a sorted array
#[t1, t2, t3...]
user = ""
database = ""
password = ""
table_and_query = []

#formulas:[[formula1, formula2, ...],
#           [formula1, ...]
#           ...
#         ]
#fs: [formula1, formula2, ...]

def process_formula(fs, tables):
    base_formula = ut.baseFormula(fs[0])
    for i, formula in enumerate(fs):
        #print(formula)
        base_formula = ut.remove1sAndAddTablename(formula, tables[i], base_formula)
    return base_formula

def get_header(query):
    parsed = sp.parse(query)[0]
    headers = (parsed.tokens)[2].value.split(",")
    output = ""
    for h in headers:
        h = h.strip()
        header = ""
        if "." in h:
            header = h.split(".")[1]
        else:
            header = h
        output += header + "   |   "
    return output

def get_index(query):
    parsed = sp.parse(query)[0]
    headers = (parsed.tokens)[2].value.split(",")
    return len(headers)


def exe_query(query, queries, tables, out_file, conn):
    
    print("executing query: " + query)
    index = get_index(query)
    result_t = None
    formulas = []
    flag = False
    for processed_query in queries:
        print("processed", processed_query, type(processed_query))
        output = conn.run(processed_query)
        #print(output)
        if len(output) == 0:
            flag = True
        if result_t == None:
            result_t = output
        for i, tu in enumerate(output):
            if len(formulas) == i:
                formulas.append([tu[index]])
            else:
                formulas[i].append(tu[index])
    print(formulas)
    processed_formulas = []
    for fs in formulas:
        #print(fs)
        rowformula = process_formula(fs, tables)
        processed_formulas.append(rowformula)
    result__ = []
    for i, row in enumerate(result_t):
        rowl = list(row)
        rowl[index] = processed_formulas[i]
        result__.append(rowl)

    # Combine rows with same output
    result = ut.combineSameRow(result__)
    
    # Now print the processed query
    header = get_header(query)
    if flag == False:
        out_file.write(query + "\n")
        out_file.write(header+ "provenance formula   |   " + "provsql-token"+"\n\n")
        for row in result:
            line = " | ".join(str(elem) for elem in row)
            out_file.write(line+"\n")
        out_file.write("\n---------------------------\n\n")
    
def get_tables_and_query():
    file = open("intermediate")
    line = file.readline()
    while line:
        line = line.strip()
        tmp1 = line.split("@")
        originQ = tmp1[0]
        processedQ = tmp1[1]
        tables_ = tmp1[2]
        processed = processedQ.split("#")
        processed = processed[:len(processed)-1]
        tables = tables_.split("#")
        tables = tables[:len(tables)-1]
        table_and_query.append([originQ, processed, tables])
        
        line = file.readline()
    file.close()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-q", "--query", help="Relative path for query file.")
    parser.add_argument("-o", "--output", help="Relative path for output file. Default as stdout")
    parser.add_argument("-d", "--database", help="Database Name")
    parser.add_argument("-u", "--user", help="Username")
    parser.add_argument("-p", "--password", help="Password")
    args = parser.parse_args() 

    queries = []
    out_file = sys.stdout
    global database
    global user
    global password
    if args.database:
        database = args.database
    if args.user:
        user = args.user
    if args.password:
        password = args.password
    if args.query:
        with open(args.query) as fp:
            line = fp.readline()
            while line:
                queries.append(line.strip())
                line = fp.readline()
    else:
        print("Please specify at least one query file.")
        exit(1)

    if args.output:
        out_file = open(args.output, 'w')
    
    conn = None
    print('connecting to databse...')
    conn = pg8000.connect(user, database=database, password=password)
    conn.autocommit = True
    conn.run('SET search_path TO public, provsql;')

    get_tables_and_query()
    
    for wrap in table_and_query:
        originQ = wrap[0]
        processed = wrap[1]
        tables = wrap[2]
        try:
            exe_query(originQ, processed, tables,  out_file, conn)
            print("FINISHED: "+originQ)
        except (Exception, pg8000.Error) as error:
            print(traceback.format_exc())
            #out_file.write(traceback.format_exc()+"\n")
            conn.commit()
            conn.close()
            conn = pg8000.connect(user, database=database, password=password)
            conn.run('SET search_path TO public, provsql;')
            
            
    print("Done!")
    out_file.close()
    if conn is not None:
        conn.close()
        print('Database connection closed.')

if __name__ == '__main__':
    main()