#!/bin/sh
dropdb provdb
createdb provdb
psql provdb < ../provsql/config/setup.sql
#psql provdb < ../provsql/config/aggregation.sql
psql provdb < provdb.sql
psql provdb < ../provsql/config/func.sql
#python3 provenance.py -q Solutions/Q6-solution.sql -o hw1-provenance/q6_solution.txt -d provdb -u joker -p m
#python3 provenance2.py -q Solutions/Q6-solution.sql -o hw1-provenance/q6_solution.txt -d provdb -u joker -p m

python3 provenance_agg.py -q Solutions/Q7-solution.sql -o hw1-provenance/q7_solution.txt -d provdb -u joker -p m
sleep 5
python3 provenance2_agg.py -q Solutions/Q7-solution.sql -o hw1-provenance/q7_solution.txt -d provdb -u joker -p m
